%     Systematic Error Computation Script
%     Copyright (C) 2019  Lukas Colombo
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU Affero General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU Affero General Public License for more details.
% 
%     You should have received a copy of the GNU Affero General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.				
				
function fehlersys(formel,varargin)
    fprintf('o-----------------------------o\n')
    fprintf('|            Start            |\n')
    fprintf('o-----------------------------o\n')
    
    
    
    %Variablen Deklaration
    sqError           = 0; %Placeholder für Fehlersummation
    fullError         = 0; %Endfehler
    meanVariableArray = cell(1, 100); %Array mit Mittelwerten
    meanErrorArray    = cell(1, 100); %Array mit Mittleren Fehlern
    
    
    %Formel und Variablennamen in der Konsole ausgeben:
    fprintf('Formel:       %s\n\n', formel)
    for i = 1:2:nargin-1
        fprintf('(Input %d)Name Variable  %d:     %s\n', i  , (i+1)/2, inputname(i+1))
        fprintf('(Input %d)Name Fehler    %d:     %s\n', i+1, (i+1)/2, inputname(i+2))
    end
    fprintf('\n')
    
    
    
    %Math Action:
    
    %Mittelwert der Variablen berechen und in der Konsole ausgeben:
    fprintf('Math:\n')
    for i = 1:2:nargin-1
        meanVariableArray{(i+1)/2} = mean(varargin{i  });
        meanErrorArray{(i+1)/2}    = mean(varargin{i+1});
        fprintf('Mittelwert für Variable %s:        %f\n',  inputname(i+1), meanVariableArray{(i+1)/2})
        fprintf('Mittlerer Fehler für Variable %s:  %f\n',  inputname(i+1),    meanErrorArray{(i+1)/2})
    end
    fprintf('\n')
    
    
    %Formel erstellen:
    for i = 1:2:nargin-1
        %Kovertieren der Variablennamen und Formel in symbolische Ausdrücke:
        symVar  = str2sym(inputname(i+1));
        symErr  = str2sym(inputname(i+2));
        f       = str2sym(formel);
        
        %Differenzieren und in der Konsole ausgeben:
        fdiff = diff(f,symVar);
        fprintf('Differenzieren der Formel "%s" nach "%s": %s\n', formel, symVar, fdiff)
        
        %fdiff mit symErr multiplizieren und in der Konsole ausgeben:
        sqError = sqError+abs(symErr*fdiff);
        fprintf('Differenzierte Formel mit mittlerem Fehler multiplizieren und ergänzen: %s\n', sqError)
    end
    fprintf('\n')
    fprintf('Endgültige Formel: %s\n\n', sqError)
    
    
    %Variablen in sqError ersetzen und evaluieren:
    fprintf('Einsetzen:\n')
    for i = 1:2:nargin-1
        sqError = subs(sqError, inputname(i+1), meanVariableArray{(i+1)/2});
        fprintf('%s\n', sqError)
        sqError = subs(sqError, inputname(i+2),    meanErrorArray{(i+1)/2});
        fprintf('%s\n', sqError)
    end
    fprintf('\n')
    
    
    %Ergebnis ausgeben:
    fullError = sqError;
    fprintf('Ergebnis: %s\n', fullError)
    
    
    
    fprintf('o-----------------------------o\n')
    fprintf('|            Ende             |\n')
    fprintf('o-----------------------------o\n')
end